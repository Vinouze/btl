#include <Arduino.h>
#define FASTLED_INTERNAL
#include <FastLED.h>


#define LED_PIN 15
#define NUM_LEDS 72
#define BRIGHTNESS 255
#define LED_TYPE WS2812B
#define COLOR_ORDER GRB
CRGB Strip[NUM_LEDS] = {0};

#define UPDATES_PER_SECOND 100

// delay
unsigned long In = 5;
unsigned long Mid = 360;
unsigned long Out = 280;

unsigned long dRearL = 600;
int fadeAmt = 4;
int brightness = 255;

void Blinker();
void RearLight(long tempo);
void Warning();
void idleLight(long tempo);
bool WayLol = 0;
bool state = 0;
bool pair = 0;

// 1 : droite; 0 : gauche
void Blinker(bool way)
{
  int index;
  int reverseIndex;
  if (way)
  {
    for (index = 0; index < 36; index++)
    {
      reverseIndex = 71 - index;
      Strip[index] = CRGB::DarkOrange;
      Strip[reverseIndex] = CRGB::DarkOrange;
      FastLED.delay(In);
      FastLED.setBrightness(65);
    }
  }
  else
  {
    for (index = 36; index >= 0; index--)
    {
      reverseIndex = 71 - index;
      Strip[index] = CRGB::DarkOrange;
      Strip[reverseIndex] = CRGB::DarkOrange; //CornflowerBlue
      FastLED.delay(In);
      FastLED.setBrightness(65);
    }
  }
  FastLED.delay(Mid);
  FastLED.clear(true);
  FastLED.delay(Out);
  FastLED.setBrightness(BRIGHTNESS);

}

void RearLight(long tempo)
{
  unsigned long condEnd = millis() + tempo;
  while (millis() <= condEnd)
  {
    for (int index = 0; index < NUM_LEDS; index++)
    {
      Strip[index] = CRGB::Red;
      Strip[index].fadeLightBy(brightness);
    }
    brightness = brightness + fadeAmt;
    if (brightness == 35 || brightness == 200)
    {
      fadeAmt = -fadeAmt;
    }
    FastLED.delay(9);
  }
  FastLED.clear(true);
}

void idleLight(long tempo)
{

  unsigned long condEnd = millis() + tempo;
  while (millis() <= condEnd)
  {

    Strip[0] = CRGB::Red;
    Strip[1] = CRGB::Red;
    Strip[70] = CRGB::Red;
    Strip[71] = CRGB::Red;

    Strip[17] = CRGB::Red;
    Strip[18] = CRGB::Red;
    Strip[53] = CRGB::Red;
    Strip[54] = CRGB::Red;

    Strip[34] = CRGB::Red;
    Strip[35] = CRGB::Red;
    Strip[36] = CRGB::Red;
    Strip[37] = CRGB::Red;

    if (state)
    {
      for (int index = 2; index < 17; index++)
      {
        Strip[index] = CRGB::Red;
      }
      for (int index = 19; index < 34; index++)
      {
        Strip[index] = CRGB::Red;
      }
      for (int index = 38; index < 53; index++)
      {
        Strip[index] = CRGB::Red;
      }
      for (int index = 55; index < 70; index++)
      {
        Strip[index] = CRGB::Red;
      }
      FastLED.show();
      delay(30);
      state = !state;
    }
    else
    {
      for (int index = 2; index < 17; index++)
      {
        Strip[index] = CRGB::Black;
      }
      for (int index = 19; index < 34; index++)
      {
        Strip[index] = CRGB::Black;
      }
      for (int index = 38; index < 53; index++)
      {
        Strip[index] = CRGB::Black;
      }
      for (int index = 55; index < 70; index++)
      {
        Strip[index] = CRGB::Black;
      }
      FastLED.show();
      if (pair)
      {
        delay(900);
        pair = !pair;
      }
      else
      {
        delay(300);
        pair = !pair;
      }
      
      state = !state;
    }
  }
  FastLED.clear(true);
}

void aircraftLight(long tempo)
{

  unsigned long condEnd = millis() + tempo;
  while (millis() <= condEnd)
  {

    Strip[0] = CRGB::Green;
    Strip[1] = CRGB::Green;
    Strip[70] = CRGB::Green;
    Strip[71] = CRGB::Green;

    // Strip[17] = CRGB::White;
    // Strip[18] = CRGB::White;
    // Strip[53] = CRGB::White;
    // Strip[54] = CRGB::White;

    Strip[34] = CRGB::Red;
    Strip[35] = CRGB::Red;
    Strip[36] = CRGB::Red;
    Strip[37] = CRGB::Red;
    FastLED.show();
  }
  FastLED.clear(true);
}

void France(long tempo)
{

  unsigned long condEnd = millis() + tempo;
  while (millis() <= condEnd)
  {
    for (int index = 0; index < 12; index++)
    {
      Strip[index] = CRGB::Red;
    }
    for (int index = 12; index < 25; index++)
    {
      Strip[index] = CRGB::FairyLight;
    }
    for (int index = 24; index < 48; index++)
    {
      Strip[index] = CRGB::Blue;
    }
    for (int index = 48; index < 60; index++)
    {
      Strip[index] = CRGB::FairyLight;
    }
    for (int index = 60; index < 72; index++)
    {
      Strip[index] = CRGB::Red;
    }
    FastLED.setBrightness(65);
    

    
    FastLED.show();
  }
  FastLED.clear(true);
  FastLED.setBrightness(BRIGHTNESS);
}

void Warning()
{
  int reverseIndex;
  int offsetIndex;
  int reverseOffsetIndex;
  for (int index = 18; index < 36; index++) // 18:35
    {
      reverseIndex = 35 - index; // 17:0
      offsetIndex = 36 + index; // 54:71
      reverseOffsetIndex = -1 * offsetIndex + 107; // 53:36

      Strip[index] = CRGB::DarkOrange;
      Strip[reverseIndex] = CRGB::DarkOrange;

      Strip[offsetIndex] = CRGB::DarkOrange;
      Strip[reverseOffsetIndex] = CRGB::DarkOrange;

      Strip[17] = CRGB::Black;
      Strip[18] = CRGB::Black;
      Strip[53] = CRGB::Black;
      Strip[54] = CRGB::Black;
      FastLED.setBrightness(65);
      FastLED.show();
      FastLED.delay(In);
    }
    FastLED.delay(Mid);
    FastLED.clear(true);
    FastLED.delay(Out);
    FastLED.setBrightness(BRIGHTNESS);
}

void setup()
{
  pinMode(2, OUTPUT);
  pinMode(LED_PIN, OUTPUT);

  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(Strip, NUM_LEDS).setCorrection(TypicalLEDStrip);

  FastLED.setBrightness(BRIGHTNESS);

  // FastLED.setMaxPowerInMilliWatts(1000);
  Serial.begin(115200);
} 

void loop()
{

  idleLight(5000);

  RearLight(3000);
  delay(500);

  Blinker(WayLol);
  Blinker(WayLol);
  Blinker(WayLol);
  WayLol = !WayLol;
  delay(500);

  Blinker(WayLol);
  Blinker(WayLol);
  Blinker(WayLol);
  WayLol = !WayLol;
  delay(500);

  Warning();
  Warning();
  Warning();
  Warning();
  delay(500);

  aircraftLight(3000);
  delay(500);

  France(2000);
  delay(500);

  // // fill_solid(Strip,NUM_LEDS,CRGB::OrangePerso);

  FastLED.show();
}
