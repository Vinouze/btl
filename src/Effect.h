#include <Arduino.h>
#define FASTLED_INTERNAL
#include <FastLED.h>

extern CRGB leds[];
extern NUM_LEDS;

void BlinkerRight();

void BlinkerRight()
{
    static byte hue = HUE_YELLOW;
    const uint8_t NbFade = 3;
    
    for (uint8_t i = 0; i < NUM_LEDS-NbFade; i++)
    {
        leds[i] = hue;
    }
    
}